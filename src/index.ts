import express, { Express, Request, Response } from 'express';
import { parse } from './semantic/parser';
import { Document } from './semantic/document';
import { compile } from './runtime/compiler';
import { evaluate } from './runtime/evaluator';

const app: Express = express();
app.use(express.text({type:"*/*"}));
const port = 3000;

app.get('/runtime/v1/run', async (req: Request, res: Response) => {
  if (req.body == undefined) {
    res.status(404).send("Empty body!");
  }

  const document = new Document(req.body);  
  parse(document).forEach(exp => {
    const newExp = `__runtime_new<${exp.typeName}>("${exp.typeName}", ${exp.values.join(", ")});`;
    document.replace(exp.range, newExp);
  });
  const compiled = await compile(document);
  const program = await evaluate(compiled);

  res.json(program.result);
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});