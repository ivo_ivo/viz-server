import { CompiledProgram } from "./compiled_program";
import { exec } from "./compiler";
import { Program, Result } from "./program";

export async function evaluate(program: CompiledProgram) {
  const {stdout, stderr} = await exec(`${program.path}`);
  console.log(stdout);
  const json: Array<Result> = JSON.parse(stdout);
  return new Program(json);
}