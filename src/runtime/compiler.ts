import { Document } from "../semantic/document";
import { promisify } from "util";
import { exec as execAsync } from "child_process";
import { writeFile as writeFileAsync } from "fs";
import { CompiledProgram } from "./compiled_program";
import { tmpNameSync } from "tmp-promise";

export const exec = promisify(execAsync);
const writeFile = promisify(writeFileAsync);

export async function compile(document: Document) {
  // TODO(ivo): make async
  const documentPath = tmpNameSync({
    postfix: ".cpp"
  });
  const programPath = tmpNameSync();

  // TODO(ivo): really bad
  document.prepend(`#include "/Users/lispberry/projects/viz-server/runtime/runtime.hpp"`);
  
  await writeFile(documentPath, document.text);
  // TODO(ivo): relative path
  const {stdout, stderr} = await exec(`clang++ -O0 -g -std=c++17 '${documentPath}' /Users/lispberry/projects/viz-server/runtime/runtime.cpp -o '${programPath}'`);

  return new CompiledProgram(programPath);
}