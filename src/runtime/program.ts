export interface Result {
  type: "List";
  address: string;
  value: number;
  next: string;
}

export class Program {
  constructor(readonly result: Array<Result>) {}
}