struct List
{
  int value;
  List *next;
};

List *append(List *head, int value)
{
  List *newVal = new List;
  newVal->value = value;
  newVal->next = nullptr; 

  if (head == nullptr)
  {
    return newVal;
  }

  head->next = newVal;
  return newVal;
}

int main()
{
  List * head = append(append(append(head, 10), 20), 30);

  return 0;
}