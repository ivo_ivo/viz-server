import { compile, exec } from "../runtime/compiler";
import { Document } from "../semantic/document";

describe("Compiler", () => {
  test("it should compile document",  async () => {
    const program = await compile(new Document(`void program() { new List{10, nullptr}; }`));
    const {stdout, stderr} = await exec(`${program.path}`);
    expect(stdout).toBe("hello world");
  });
});