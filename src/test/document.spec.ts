import { Document } from "../semantic/document";

describe("Document", () => {
  test("it should have replace", () => {
    const document = new Document(`
int main() {
  return 0;
}`);
  
    document.replace({
      start: {
        line: 1,
        column: 4,
      },
      end: {
        line: 1,
        column: 7
      } 
    }, "hello");
  
  expect(document.text).toBe(`
int hello() {
  return 0;
}`);
  });

  test("it should have query", () => {
    const document = new Document(`
int main() {
  auto val = new int(10, 20);
  return 0;
}`);
  
    const nodes = document.query("(new_expression) @new")
      .flatMap(pattern => pattern.captures)
      .map(capture => capture.node)
      .forEach(node => {
        document.replace(Document.toRange(node.startPosition, node.endPosition), `test;`);
      });
  
    expect(document.text).toBe(`
int main() {
  auto val = test;
  return 0;
}`);
  });
})