import { Document } from "./document";
import { NewExp } from "./exp";

export function parse(document: Document) {
  return document.query("(new_expression) @news")
    .flatMap(pattern => pattern.captures)
    .map(capture => capture.node)
    .map(node => {
      const range = Document.toRange(node.startPosition, node.endPosition);
      const typeName = node.namedChild(0)?.text ?? "";
      const values = node.namedChild(1)?.namedChildren
        ?.map(child => child.text) ?? [];

      return new NewExp(range, typeName, values);
    }); 
}