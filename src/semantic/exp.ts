import { Document } from "./document";

export interface Exp {
  readonly type: "NewExp";
  range: Document.Range;
}

export class NewExp implements Exp { 
  readonly type = "NewExp";

  constructor(readonly range: Document.Range, readonly typeName: string, readonly values: string[]) {}
}

export type TypedExp = NewExp;