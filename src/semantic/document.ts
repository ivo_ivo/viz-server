import Parser, { Query } from "tree-sitter";

export class Document {
  private _lines = new Array<string>();
  private readonly _parser = new Parser();
  private readonly _cpp = require("tree-sitter-cpp");
  
  constructor(private readonly _text: string) {
    this._lines = this._text.split('\n');
    this._parser.setLanguage(this._cpp);
  }

  get text() {
    return this._lines.join('\n');
  }

  get ast() {
    return this._parser.parse(this.text);
  }

  query(input: string) {
    const req = new Query(this._cpp, input);
    return req.matches(this.ast.rootNode);
  }
  
  prepend(text: String) {
    const lines = text.split('\n');
    this._lines = [...lines, ...this._lines];
  }

  replace(range: Document.Range, text: String) {
    if (range.start.line == range.end.line)
    {
      const line = this._lines[range.start.line]
      const start = line.slice(0, range.start.column)
      const end = line.slice(range.end.column + 1, line.length);

      this._lines[range.start.line] = start + text + end
      return
    }
  }

  static toPosition(point: Parser.Point): Document.Position {
    return {
      line: point.row,
      column: point.column
    };
  }

  static toRange(start: Parser.Point, end: Parser.Point): Document.Range {
    return {
      start: Document.toPosition(start),
      end: Document.toPosition(end)
    };
  }
}

export namespace Document {
  export interface Position {
    line: number;
    column: number;
  }
  
  export interface Range {
    start: Position;
    end: Position;
  }
}