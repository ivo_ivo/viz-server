import { NewExp } from "./exp";
import { Pass } from "./pass";

export const runtimePass: Pass<Array<NewExp>, Array<string>> = (exps) => {
  return exps.map(exp => `__new_runtime<${exp.typeName}>("${exp.typeName}", ${exp.values.join(", ")});`);
};