#pragma once

#include <string_view>
#include <string>
#include <iostream>
#include <vector>

struct Type
{
  std::string typeName;
  uintptr_t address;
};

extern std::vector<Type> g_types;

template <typename T, typename ...Args>
auto __runtime_new(std::string_view typeName, Args&& ...args)
{
  auto mem = new T{std::forward<Args>(args)...};
  g_types.push_back(Type{
    .typeName = std::string(typeName),
    .address = reinterpret_cast<uintptr_t>(mem)
  });

  return mem;
}

struct List {
  int value;
  List *next;
};

inline auto __runtime_print()
{
  std::cout << "[" << std::endl;
  for (size_t i = 0; i < g_types.size(); i++)
  {
    auto const & [typeName, address] = g_types[i];

    if (typeName == "List")
    {
      List *list = reinterpret_cast<List *>(address);

      std::cout << "{\"type\": " 
        << '"' << typeName << '"' 
        << ", " 
        << "\"address\": " << '"' << std::hex << address << '"' << ","
        << "\"value\": " << std::dec << list->value << ","
        << "\"next\": " << '"' << std::hex << list->next << '"';


        if (i == g_types.size() - 1)
        {
          std::cout << "}" << std::endl;
        }
        else
        {
          std::cout << "}," << std::endl;
        }
    }
  }
  std::cout << "]" << std::endl;
}